package com.itau.bigcase.emissaonfe.clients;

import com.itau.bigcase.emissaonfe.exceptions.IdentidadeInvalidaException;
import com.itau.bigcase.emissaonfe.exceptions.NotaFiscalNotFoundException;
import feign.codec.ErrorDecoder;

import feign.Response;

public class NotaFiscalClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new NotaFiscalNotFoundException();
        } else if (response.reason().contains("Identidade") || s.contains("IdentidadeInvalida")) {
            return new IdentidadeInvalidaException();
        }  else
        {
            return errorDecoder.decode(s, response);
        }
    }
}

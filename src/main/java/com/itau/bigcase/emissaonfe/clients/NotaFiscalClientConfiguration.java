package com.itau.bigcase.emissaonfe.clients;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class NotaFiscalClientConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new NotaFiscalClientErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(NotaFiscalClientFallback::new)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
package com.itau.bigcase.emissaonfe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.NOT_FOUND, reason="Entidade não encontrada")
public class NotaFiscalNotFoundException extends  RuntimeException{
}

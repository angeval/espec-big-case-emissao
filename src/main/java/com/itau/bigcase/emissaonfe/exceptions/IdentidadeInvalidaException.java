package com.itau.bigcase.emissaonfe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_REQUEST, reason="Identidade informada não é um CPF nem CNPJ válido")
public class IdentidadeInvalidaException extends  RuntimeException{
}

package com.itau.bigcase.emissaonfe.models.dtos;

public class CreateNotaFiscalResponse {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

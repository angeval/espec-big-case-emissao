package com.itau.bigcase.emissaonfe.models.dtos;

import java.math.BigDecimal;

public class CreateNotaFiscalRequest {

        private String identidade;

        private BigDecimal valor;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}

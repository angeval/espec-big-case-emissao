package com.itau.bigcase.emissaonfe.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class NotaFiscal {
    @Id
    private String identidade;

    private BigDecimal valorTotal;

    private String status;

    @OneToOne
    private Nfe nfe;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Nfe getNfe() {
        return nfe;
    }

    public void setNfe(Nfe nfe) {
        this.nfe = nfe;
    }
}

package com.itau.bigcase.emissaonfe.services;

import com.itau.bigcase.emissaonfe.exceptions.IdentidadeInvalidaException;
import com.itau.bigcase.emissaonfe.exceptions.NotaFiscalNotFoundException;
import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import com.itau.bigcase.emissaonfe.producer.LogProducer;
import com.itau.bigcase.emissaonfe.producer.NotaFiscalProducer;
import com.itau.bigcase.emissaonfe.repositories.NotaFiscalRepository;
import com.itau.bigcase.emissaonfe.utils.CpfCnpjUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class NotaFiscalService {
    @Autowired
    private NotaFiscalRepository notaFiscalRepository;

    @Autowired
    private NotaFiscalProducer producer;

    @Autowired
    private LogProducer logProducer;

    public List<NotaFiscal> buscarTodasNotasFiscais(String identidade) {
        validarIdentidade(identidade);
        List<NotaFiscal> notaFiscalLista = notaFiscalRepository.findAllByIdentidade(identidade);
        if(notaFiscalLista.isEmpty()){
            throw new NotaFiscalNotFoundException();
        }
        String log = "["+formatarDataHoraLog()+"] [Consulta]: "+identidade+" acaba de pedir os dados das suas notas fiscais.";
        logProducer.enviarLogAoKafka(log);
        return notaFiscalLista;
    }

    public NotaFiscal criarNotaFiscal(NotaFiscal notaFiscal) {
        validarIdentidade(notaFiscal.getIdentidade());
        notaFiscal.setStatus("pending");
        notaFiscal = notaFiscalRepository.save(notaFiscal);
        producer.enviarAoKafka(notaFiscal);

        String log = "["+formatarDataHoraLog()+"] [Emissão]: "+notaFiscal.getIdentidade()+" acaba de pedir a emissão de uma NF no valor de R$ "
                +notaFiscal.getValorTotal()+"!";
        logProducer.enviarLogAoKafka(log);

        return notaFiscal;
    }

    public NotaFiscal buscarNotaFiscal(Long id){
        Optional<NotaFiscal> notaFiscalOptional = notaFiscalRepository.findByNfe_Id(id);
        if (notaFiscalOptional.isPresent()) {
            return notaFiscalOptional.get();
        }
        else{
            throw new NotaFiscalNotFoundException();
        }
    }

    public void validarIdentidade(String identidade){
        if (!CpfCnpjUtils.isValid(identidade)){
            throw new IdentidadeInvalidaException();
        }
    }

    public String formatarDataHoraLog(){
        Calendar cal = Calendar.getInstance();
        Timestamp time = new Timestamp(cal.getTimeInMillis());
        return time.toString();
    }
}

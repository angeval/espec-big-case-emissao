package com.itau.bigcase.emissaonfe.controllers;

import com.itau.bigcase.emissaonfe.exceptions.IdentidadeInvalidaException;
import com.itau.bigcase.emissaonfe.mappers.NotaFiscalMapper;
import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import com.itau.bigcase.emissaonfe.models.dtos.CreateNotaFiscalRequest;
import com.itau.bigcase.emissaonfe.models.dtos.CreateNotaFiscalResponse;
import com.itau.bigcase.emissaonfe.security.Usuario;
import com.itau.bigcase.emissaonfe.services.NotaFiscalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/nfe")
public class NotaFiscalController {

    @Autowired
    private NotaFiscalService notaFiscalService;

    @PostMapping("/emitir")
    @ResponseStatus(code= HttpStatus.CREATED)
    public CreateNotaFiscalResponse emitirNotaFiscal( @RequestBody @Valid CreateNotaFiscalRequest createNotaFiscalRequest
                                                    , @AuthenticationPrincipal Usuario usuario){
        NotaFiscalMapper notaFiscalMapper = new NotaFiscalMapper();
        NotaFiscal notaFiscal = notaFiscalMapper.fromCreateRequest(createNotaFiscalRequest);
        try {
            notaFiscal = notaFiscalService.criarNotaFiscal(notaFiscal);
            return notaFiscalMapper.toCreateResponse(notaFiscal);
        } catch (IdentidadeInvalidaException e){
            throw new IdentidadeInvalidaException();
        }
    }

    @GetMapping("/consultar/{identidade}")
    public List<NotaFiscal> consultarNotasFiscais(@PathVariable String identidade
                                                , @AuthenticationPrincipal Usuario usuario){
        List<NotaFiscal> nfeLista = notaFiscalService.buscarTodasNotasFiscais(identidade);
        return nfeLista;
    }
}

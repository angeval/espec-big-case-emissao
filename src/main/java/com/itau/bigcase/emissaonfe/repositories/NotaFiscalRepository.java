package com.itau.bigcase.emissaonfe.repositories;

import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface NotaFiscalRepository extends CrudRepository<NotaFiscal, String> {
    List<NotaFiscal> findAllByIdentidade(String identidade);

    Optional<NotaFiscal> findByNfe_Id(Long id);
}

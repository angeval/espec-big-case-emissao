package com.itau.bigcase.emissaonfe.mappers;

import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import com.itau.bigcase.emissaonfe.models.dtos.CreateNotaFiscalRequest;
import com.itau.bigcase.emissaonfe.models.dtos.CreateNotaFiscalResponse;
import org.springframework.stereotype.Component;

@Component
public class NotaFiscalMapper {
    public NotaFiscal fromCreateRequest(CreateNotaFiscalRequest createNotaFiscalRequest) {
        NotaFiscal notaFiscal = new NotaFiscal();
        notaFiscal.setIdentidade(createNotaFiscalRequest.getIdentidade());
        notaFiscal.setValorTotal(createNotaFiscalRequest.getValor());

        return notaFiscal;
    }

    public CreateNotaFiscalResponse toCreateResponse(NotaFiscal notaFiscal) {
        CreateNotaFiscalResponse createNotaFiscalResponse = new CreateNotaFiscalResponse();
        createNotaFiscalResponse.setStatus(notaFiscal.getStatus());
        return createNotaFiscalResponse;
    }
}

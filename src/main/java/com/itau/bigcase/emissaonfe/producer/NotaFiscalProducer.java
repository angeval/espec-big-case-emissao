package com.itau.bigcase.emissaonfe.producer;

import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotaFiscalProducer {
    @Autowired
    private KafkaTemplate<String, NotaFiscal> producer;

    public void enviarAoKafka(NotaFiscal notaFiscal) {
        producer.send("spec2-angela-valentim-1", notaFiscal);
    }

}
